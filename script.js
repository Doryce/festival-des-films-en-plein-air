
$(document).ready(function() {
    /* ======================
#NAVIGATION
========================= */
	   
    // JQUERY NAV TOGGLE
    $('#menu').bind('click',function(event){
        $('#mainnav ul').slideToggle();
    });

    $(window).resize(function(){  
        var w = $(window).width();  
        if(w > 768) {  
            $('#mainnav ul').removeAttr('style');  
        }  
    });
}); 
/* ======================
#CAROUSEL
========================= */



// Slide arrow controls
$(".arrow-left").click(function() {
    $(this).addClass("hide");
    $('.slides').toggleClass("move");
    $('.arrow-right').removeClass("hide");
    $('.dot-left').removeClass("inactive").addClass("active");
    $('.dot-right').removeClass("active").addClass("inactive");
})

$(".arrow-right").click(function() {
    $(this).addClass("hide");
    $('.slides').toggleClass("move");
    $('.arrow-left').removeClass("hide");
    $('.dot-left').removeClass("active").addClass("inactive");
    $('.dot-right').removeClass("inactive").addClass("active");
})

// Slide dot controls
$(".dot-left").click(function() {
    $(this).toggleClass("active").toggleClass("inactive");
    $('.slides').toggleClass("move");
    $('.arrow').toggleClass("hide");
    $('.dot-right').toggleClass("active").toggleClass("inactive");
})

$(".dot-right").click(function() {
    $(this).toggleClass("active").toggleClass("inactive");
    $('.slides').toggleClass("move");
    $('.arrow').toggleClass("hide");
    $('.dot-left').toggleClass("active").toggleClass("inactive");
})



// Slide timer
var timer;

function startTimer() {
    timer = setInterval(function () {
      $('.slides').toggleClass("move");
      $('.arrow-left').toggleClass("hide");
      $('.arrow-right').toggleClass("hide");
      $('.navi-dot').toggleClass("active").toggleClass("inactive");
    }, 6000);
}

$('.carousel').hover(function (ev) {
    clearInterval(timer);
}, function (ev) {
    startTimer();
});
startTimer();

/* ======================
QUIZ
========================= */


(function() {
    function buildQuiz() {
      // we'll need a place to store the HTML output
      const output = [];
  
      // for each question...
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // we'll want to store the list of answer choices
        const answers = [];
  
        // and for each available answer...
        for (letter in currentQuestion.answers) {
          // ...add an HTML radio button
          answers.push(
            `<label>
              <input type="radio" name="question${questionNumber}" value="${letter}">
              ${letter} :
              ${currentQuestion.answers[letter]}
            </label>`
          );
        }
  
        // add this question and its answers to the output
        output.push(
          `<div class="question"> ${currentQuestion.question} </div>
          <div class="answers"> ${answers.join("")} </div>`
        );
      });
  
      // finally combine our output list into one string of HTML and put it on the page
      quizContainer.innerHTML = output.join("");
    }
  
    function showResults() {
      // gather answer containers from our quiz
      const answerContainers = quizContainer.querySelectorAll(".answers");
  
      // keep track of user's answers
      let numCorrect = 0;
  
      // for each question...
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // find selected answer
        const answerContainer = answerContainers[questionNumber];
        const selector = `input[name=question${questionNumber}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;
  
        // if answer is correct
        if (userAnswer === currentQuestion.correctAnswer) {
          // add to the number of correct answers
          numCorrect++;
  
          // color the answers green
          answerContainers[questionNumber].style.color = "lightgreen";
        } else {
          // if answer is wrong or blank
          // color the answers red
          answerContainers[questionNumber].style.color = "red";
        }
      });
  
      // show number of correct answers out of total
      resultsContainer.innerHTML = `${numCorrect} out of ${myQuestions.length}`;
    }
  
    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit");
    const myQuestions = [
      {
        question: "question1?",
        answers: {
          a: "1",
          b: "2",
          c: "3"
        },
        correctAnswer: "c"
      },
      {
        question: "question2?",
        answers: {
          a: "1",
          b: "2",
          c: "3"
        },
        correctAnswer: "c"
      },
      {
        question: "question3?",
        answers: {
          a: "1",
          b: "2",
          c: "3",
        },
        correctAnswer: "c"
      }
    ];
  
    // display quiz right away
    buildQuiz();
  
    // on submit, show results
    submitButton.addEventListener("click", showResults);
  })();